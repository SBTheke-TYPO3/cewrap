<?php

namespace SBTheke\Cewrap\ExpressionLanguage;

use Symfony\Component\ExpressionLanguage\ExpressionFunction;
use Symfony\Component\ExpressionLanguage\ExpressionFunctionProviderInterface;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

class UtilitiesConditionFunctionsProvider implements ExpressionFunctionProviderInterface
{
    /**
     * @return ExpressionFunction[] An array of function instances
     */
    public function getFunctions(): array
    {
        return [
            $this->getExtensionLoadedFunction(),
            $this->getExtensionVersion()
        ];
    }

    protected function getExtensionLoadedFunction(): ExpressionFunction
    {
        return new ExpressionFunction('cewrapExtensionIsLoaded', function () {
            // Not implemented, we only use the evaluator
        }, function ($arguments, $extKey) {
            return ExtensionManagementUtility::isLoaded($extKey);
        });
    }

    protected function getExtensionVersion(): ExpressionFunction
    {
        return new ExpressionFunction('cewrapExtensionMainVersion', function () {
            // Not implemented, we only use the evaluator
        }, function ($arguments, $extKey, $version) {
            return version_compare(
                ExtensionManagementUtility::getExtensionVersion($extKey),
                $version
            ) !== -1;
        });
    }
}
