<?php
defined('TYPO3') || defined('TYPO3_MODE') || die('Access denied.');

// Add static template
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('cewrap', 'Configuration/TypoScript/', 'Content element wrap');
