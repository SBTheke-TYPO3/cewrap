<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "cewrap".
 *
 * Auto generated 26-12-2023 10:22
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'Content element wrap',
  'description' => 'Allows to wrap content elements with individual IDs and/or classes and select multiple predefined classes, e.g. to hide content elements dependent to screen width.',
  'category' => 'fe',
  'version' => '5.0.3',
  'state' => 'stable',
  'uploadfolder' => false,
  'clearcacheonload' => true,
  'author' => 'Sven Burkert',
  'author_email' => 'bedienung@sbtheke.de',
  'author_company' => 'SBTheke web development',
  'constraints' => 
  array (
    'depends' => 
    array (
      'typo3' => '10.4.0-12.4.99',
    ),
    'suggests' => 
    array (
      'cefooter' => '',
      'fluid_styled_content' => '',
      'bootstrap_package' => '14.0.0-',
      'gridelements' => '',
    ),
    'conflicts' => 
    array (
    ),
  ),
);

