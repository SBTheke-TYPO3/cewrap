<?php
defined('TYPO3') || defined('TYPO3_MODE') || die('Access denied.');

// Registering CSH (Context Sensitive Help)
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tt_content', 'EXT:cewrap/Resources/Private/Language/locallang_csh.xlf');
